/* --------------------------------------------------

	ui
	
-------------------------------------------------- */
var UI = (function ($) {
    var jk = {};
    jk.config = {
    	winW: $(window).outerWidth(),
    	winH: $(window).outerHeight(),
    	$viewport: $(".viewport"),
    	$nvpMenu_toggle: $(".menu"),
    };
    jk.init = function () {
    	//jk.window();
    	jk.socialblend();
    	jk.ui.nvpMenu();

    };
    jk.viewport = function () {
    	var h = jk.config.winH;    	
    	
    };
    jk.window = function () {
    	var $window = $(window);
    	
    	$window.resize(function () {
    		jk.config.winH = $window.outerHeight();
    		jk.viewport();
    	});
    }
  	jk.ui = {
  		nvpMenu : function () {
	  		var _viewport = jk.config.$viewport,
	  			_toggle = jk.config.$nvpMenu_toggle;	  		
	  		_toggle.on("click", this, function(e) {
	  			e.preventDefault();
	  			_viewport.toggleClass("shift");
	  		});
	  		
  		}

  	};
    jk.socialblend = function() {
    	var $feed = $("#socialblend");
		if ($feed.length) {
			var config = { 
				container : $feed.attr("data-container"),
				count : $feed.attr("data-count"),
				limit : $feed.attr("data-limit"),
				instaID : $feed.attr("data-instaID"),
				twitterName: $feed.attr("data-twitterName"),
				hashtag: $feed.attr("data-hashtag")
			};
			socialblend.init({
				path: "_socialblend/",
				container: $(config.container),
				count: config.count,
				limit: config.limit,
				instaID: config.instaID,
				instaHash: config.hashtag,
				twitterName: config.twitterName,
				twitterSearch: "#"+ config.hashtag+" OR @"+config.hashtag
			});
			eux.config.$panels = $(".panel");
			eux.config.$containers = $(".container");
		}
    };
  	jk.scrollPage = function (i, o, d) {
		var y = $(i).offset().top - o;
  		$('html, body').animate({
			scrollTop: y
		}, {duration: d, queue: false});
  	};
    return jk;
})(jQuery);

/*
	start it up
*/
// ################################################################################
$(function () {

	UI.init();


});