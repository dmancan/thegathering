// necessary FUNCTIONS for TWITTER stuff
// thanks to those who've been stolen from
// ################################################################################
function sortDescending(a, b) {
	var date1  = $(a).find(".year").text();
	date1 = date1.split('-');
	date1 = new Date(date1[2], date1[1] -1, date1[0]);
	var date2  = $(b).find(".year").text();
	date2= date2.split('-');
	date2= new Date(date2[2], date2[1] -1, date2[0]);
	
	return date1 < date2 ? 1 : -1;
};
function linkify(str){
    // order matters
    var re = [
        "\\b((?:https?|ftp)://[^\\s\"'<>]+)\\b",
        "\\b(www\\.[^\\s\"'<>]+)\\b",
        "\\b(\\w[\\w.+-]*@[\\w.-]+\\.[a-z]{2,6})\\b",
       	"#([a-z0-9]+)",
        "@([a-z0-9]+)" ];
    re = new RegExp(re.join('|'), "gi");

    return str.replace(re, function(match, url, www, mail, hashtag, tweeter){
        if(url)
            return "<a href=\"" + url + "\">" + url + "</a>";
        if(www)
            return "<a href=\"http://" + www + "\">" + www + "</a>";
        if(mail)
            return "<a href=\"mailto:" + mail + "\">" + mail + "</a>";
        if(hashtag)
            return "<a href=\"https://twitter.com/search?src=hash&q=%23" + hashtag + "\">#" + hashtag + "</a>";
        if(tweeter)
            return "<a href=\"https://twitter.com/" + tweeter + "\">@" + tweeter + "</a>";

        // shouldnt get here, but just in case
        return match;
    });
}
var formatDate = {
	extract_relative_time : function (date) {
        var relative_to = new Date(),
            delta = (relative_to.getTime() - date) / 1000;

        return {
            days: Math.floor(delta / 86400),
            hours: Math.floor(delta / 3600),
            minutes: Math.floor(delta / 60),
            seconds: Math.floor(delta)
        };
    },
    format_relative_time : function (time_ago) {
        if (time_ago.days > 2) { return time_ago.days + ' days ago'; }
        if (time_ago.hours > 24) { return '1 day ago'; }
        if (time_ago.hours > 2) { return time_ago.hours + ' hours ago'; }
        if (time_ago.minutes > 45) { return '1 hour ago'; }
        if (time_ago.minutes > 2) { return time_ago.minutes + ' minutes ago'; }
        if (time_ago.seconds > 1) { return time_ago.seconds + ' seconds ago'; }
        return 'just now';
    }
}
// ################################################################################
// grab tweets
var tweets = {
	init : function (options, target, api) {
		this.options = options;
		this.target = target;
		this.api = api;
		
		this.build(options);
	},
	load : function () {
		var obj;
		$.ajaxSetup({ async: false });
		$.getJSON(
			this.api, 
			this.options,
			function(data){
				obj = data;
			}
			).error(function() { 
				//console.log("it blew up"); 
			}
		);	
		
		return obj;
	},
	build : function () {
		var data = 	tweets.load();
		var $target = this.target;
		
		$.each(data, function(index, item) {
			
			if ((item.entities.media)) {
				var media = item.entities.media[0].media_url;
			}
			var ts = Math.round((new Date(item.created_at)));
			
			var tweet = {
				text				: item.text,
				time				: formatDate.format_relative_time(formatDate.extract_relative_time(new Date(item.created_at))),
				timestamp			: ts,
				media				: media,
				user				: item.user
			};
			
			$target.append(Mustache.to_html($("#tweet").html(), tweet));
		});
	}
}

// grab instagrams
var instagram = {
	init : function (options, target, api) {
		this.url = this.composeRequestURL(options);
		this.api = api;
		this.target = target;
		this.build();
	},
	composeRequestURL : function (options) {
		var apiEndpoint = "https://api.instagram.com/v1";
       	var settings = {
			hash: null
          	, userId: null
          	, locationId: null
          	, search: null
          	, accessToken: null
          	, clientId: null
          	, show: null
          	, onLoad: null
          	, onComplete: null
          	, maxId: null
          	, minId: null
          	, next_url: null
        };
        options && $.extend(settings, options);
		var url = apiEndpoint;
		var params = {};
		
		if (settings.next_url != null) {
			return settings.next_url;
		}
		
		if(settings.hash) {
			url += "/tags/" + settings.hash + "/media/recent";
		}
		else if(settings.search != null) {
			url += "/media/search";
			params.lat = settings.search.lat;
			params.lng = settings.search.lng;
			settings.search.max_timestamp != null && (params.max_timestamp = settings.search.max_timestamp);
			settings.search.min_timestamp != null && (params.min_timestamp = settings.search.min_timestamp);
			settings.search.distance != null && (params.distance = settings.search.distance);
		}
		else if(settings.userId != null) {
			url += "/users/" + settings.userId + "/media/recent";
		}
		else if(settings.locationId != null) {
			url += "/locations/" + settings.locationId + "/media/recent";
		}
		else {
			url += "/media/popular";
		}
		
		settings.accessToken != null && (params.access_token = settings.accessToken);
		settings.clientId != null && (params.client_id = settings.clientId);
		settings.minId != null && (params.min_id = settings.minId);
		settings.maxId != null && (params.max_id = settings.maxId);
		settings.show != null && (params.count = settings.show);
		
		url += "?" + $.param(params)
		
		return url;
    },
	load : function () {
		var obj;
		$.ajaxSetup({ async: false });
		$.getJSON(
			this.api, 
			{
    			url: this.url
 		 	},
			function(data){
				obj = data;
			}
			).error(function() { 
				//console.log("it blew up"); 
			}
		);	
		return obj;
	},
    build : function () {
		var data = this.load();
		var $target = this.target;
		//console.log(data);
		
		$.each(data.data, function(index, item) {
			var date = + (item.created_time * 1000);
			
			var insta = {
				text				: item.caption,
				link				: item.link,
				src					: item.images.standard_resolution.url,
				time				: formatDate.format_relative_time(formatDate.extract_relative_time(date)),
				timestamp			: date,
				user				: item.user,
				location			: location
			};
			$target.append(Mustache.to_html($("#insta").html(), insta));
		});
	}
}

var socialblend = {
	init : function (options) {
		this.options = options;
		var $tweets = $("<div>");
		var	$instas = $("<div>");
		this.tweets = $tweets;
		this.instas = $instas;
		this.grab();
		this.jam(this.merge());
		this.tidy();
	},
	tidy : function () {
		// !! needs improving
		// cleanup and formating
		// this is a wee bit lame, but works
		var msg = this.options.container.find(".msg");
		$.each(msg, function(index, item) {
			var str = $(item).html();
			$(item).html(linkify(str));
		});
	},
	merge : function () {
		var $t = this.tweets;
		var $i = this.instas;
		var stream = $.map($t.children(".panel"), function(v, i) { return [v, $i.children(".panel")[i]]; });
		//var stream = $.merge($t, $i);
		return stream; 
	},
	jam : function (stream) {
		// just get as many as we asked for (count for each)
		var output = $(stream).splice(0, this.options.limit);

		$(this.options.container).html(stream);
		EUX.viewport();
	},
	grab : function () {
		// loaders
		tweets.init({
			screen_name: this.options.twitterName,
			q: this.options.twitterSearch,
			count: this.options.count,
		}, this.tweets, this.options.path + "get-tweets.php");
		
		instagram.init({
			userId: this.options.instaID,
			hash: this.options.instaHash,
			show : this.options.count
		}, this.instas, this.options.path + "get-instagram.php");
	}
	
}

	



