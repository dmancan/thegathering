thegathering
============

The Gathering 2014 Teaser Site

socialblend.js... when going production, we need a new app key for Insta (and Twitter?) or should we use one "master EUX" app (for client_ID's and secret keys, etc)?

socialblend polls the social networks and cache's the JSON results (time cadence can be changed somewhere).

JK: my vision for socialblend was a seperate application that sits independent of any site (so each individual project doesn't require the PHP caching... just a call to the cached JSON).
